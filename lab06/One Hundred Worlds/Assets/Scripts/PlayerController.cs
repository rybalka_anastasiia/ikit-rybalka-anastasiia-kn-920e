using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Joystick joystick;
    public float WalkSpeed = 0.5f;
    public float RotationSpeed = 2f;
    public float AttackDistance = 5f;

    void Start()
    {

    }

    void Update()
    {
        gameObject.transform.Translate(0, 0, joystick.Vertical * WalkSpeed);
        gameObject.transform.Rotate(0, joystick.Horizontal * RotationSpeed, 0);
    }

    public void Attack()
    {
        // ������� ��� ������ �� ��������� ������
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.forward * AttackDistance);

        // ��������� ����������� ���� � ��������� �� �������� ����������
        if (Physics.Raycast(ray, out hit, AttackDistance))
        {
            Debug.Log(hit);
            // �������� ��������� EnemyAI �� ������������� �������
            EnemyAI enemyAI = hit.collider.GetComponent<EnemyAI>();

            // ���������, ��� ������ ����� ��������� EnemyAI
            if (enemyAI != null)
            {
                // ������ Attack = true � ���������� EnemyAI
                enemyAI.Attack = true;
            }
        }
    }
}