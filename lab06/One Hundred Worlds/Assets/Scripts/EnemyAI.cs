using UnityEngine;

public class EnemyAI : MonoBehaviour
{
	public float movementSpeed = 3f; // �������� �������� �����
	public float detectionRange = 10f; // ��������� ����������� ���������
	public float attackRange = 2f; // ��������� ����� ���������
	public float attackDelay = 1f;
	public ParticleSystem particleSystem;
	public bool Attack = false;
	private Transform target; // ��������, �� ������� ������ ����
	private float lastAttackTime;

	private Animator animator;


	private void Start()
	{
		target = GameObject.FindGameObjectWithTag("Player").transform; // ����� ��������� �� ���� "Player"
		animator = GetComponent<Animator>();
	}

	private void Update()
	{
		

		// ���������� ����� ������ � ����������
		float distanceToTarget = Vector3.Distance(transform.position, target.position);

		if (Attack && distanceToTarget < detectionRange)
		{
			// ���� ������� ���������
			if (distanceToTarget > attackRange)
			{
				// ���� ��������� ��� ��������� �����, ������� �� ��������� � ���������
				Vector3 targetDirection = target.position - transform.position;
				targetDirection.y = 0; // ������������� ������� ������, ����� ���� �� ����������
				transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection), 0.1f); // ������������ ����� � ����������� ���������
				transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime); // ������� ����� ������
				animator.SetBool("isAttack", false);
				animator.SetBool("isWalk", true);
                lastAttackTime = 0f;
            }
			else
			{
                // ���� ��������� � ��������� �����, ������� �� ������� ���������
                FAttack();
			}
		}
		else {
			animator.SetBool("isAttack", false);
			animator.SetBool("isWalk", false);
			Attack = false;
		}
	}

	private void FAttack()
	{
		animator.SetBool("isWalk", false);
		animator.SetBool("isAttack", true);
		lastAttackTime += Time.deltaTime;
		if (lastAttackTime >= attackDelay)
		{
			particleSystem.Play();
			lastAttackTime = 0f; // �������� ������ ������� �� 0
		}
}
}
