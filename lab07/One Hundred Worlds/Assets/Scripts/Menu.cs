using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void ToGame()
    {
        SceneManager.LoadScene("WorkScene");
    }
    public void ToMenu()
    {
        SceneManager.LoadScene("StartScene");
    }
    public void ToExit()
    {
        Application.Quit();
    }
}
